FROM rust:1.85 AS librespot

RUN apt-get update \
 && apt-get -y install build-essential portaudio19-dev curl unzip \
 && apt-get clean && rm -fR /var/lib/apt/lists

ARG ARCH=amd64
ARG LIBRESPOT_VERSION=v0.6.0

COPY ./compile-librespot.sh /tmp/
RUN /tmp/compile-librespot.sh

#COPY ./install-librespot.sh /tmp/
#RUN /tmp/install-librespot.sh

FROM debian:12.9

ARG ARCH=amd64
ARG SNAPCAST_VERSION=v0.31.0

RUN apt update \
 && apt install -y --no-install-recommends ca-certificates curl libasound2 mpv libavahi-compat-libdnssd1 \
 && export SNAPCAST_VERSION=$(echo ${SNAPCAST_VERSION} | tr -d "v") \
 && echo "Downloading https://github.com/badaix/snapcast/releases/download/v${SNAPCAST_VERSION}/snapserver_${SNAPCAST_VERSION}-1_${ARCH}_bookworm.deb" \
 && curl -L -o /tmp/snapserver.deb "https://github.com/badaix/snapcast/releases/download/v${SNAPCAST_VERSION}/snapserver_${SNAPCAST_VERSION}-1_${ARCH}_bookworm.deb" \
 && dpkg -i /tmp/snapserver.deb || apt install -f -y --no-install-recommends \
 && apt-get clean && rm -fR /var/lib/apt/lists

COPY --from=librespot /librespot/target/release/librespot /usr/local/bin/
#COPY --from=librespot /usr/local/cargo/bin/librespot /usr/local/bin/

CMD ["snapserver", "-c", "/etc/snapserver.conf"]


ENV DEVICE_NAME=Snapcast
EXPOSE 1704/tcp 1705/tcp
